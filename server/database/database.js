const spicedPg = require('spiced-pg');
let password = require('./password')

const dbUrl = process.env.DATABASE_URL || `postgres:${password.id}:${password.password}@localhost:5432/codechallengemathieu`;
const db = spicedPg(dbUrl);

const registerUser = (userName) => {
    return new Promise(function (resolve, reject) {
        const query = `INSERT INTO users (username)
        VALUES ($1)
        RETURNING id, username;`;

        const params = [userName];
        return db.query(query, params).then(function (results) {
            resolve(results.rows[0])
        }).catch(function (e) {
            reject(e);
        });
    })
};

const login = (userName) => {
    return new Promise(function (resolve, reject) {
        const query = `SELECT * FROM users WHERE username =$1;`
        ;

        if (password == "") {
            password = null;
        }

        const params = [userName || null];
        return db.query(query, params).then(function (results) {
            resolve(results.rows[0]);
        }).catch(function (e) {
            reject(e);
        })
    })
}

const addBook = ({isbn, userId, review, rate, title}) => {
    return new Promise((resolve, reject) => {
        const query =
            `with new_book as (
      INSERT into books (ISBN, creator_id, title)
      VALUES ($1, $2, $3)
      RETURNING id
    )
    INSERT into book_reviews (book_id, review, reviewer_id, rate)
    VALUES ((select id from new_book), $4, $2, $5)
    RETURNING book_id;`;

        const params = [isbn, userId, title, review, rate];
        return db.query(query, params).then(function (results) {
            resolve(results.rows[0]);
        }).catch(function (e) {
            reject(e);
        })
    })
};


const editReview = ({review, rate, bookId, userId}) => {
    return new Promise((resolve, reject) => {
        const query =
            `UPDATE book_reviews
    SET review=$1,
    rate=$2
    WHERE book_id=$3 AND reviewer_id=$4
    RETURNING book_id;`;

        const params = [review, rate, bookId, userId];
        return db.query(query, params).then(function (results) {
            resolve(results.rows[0]);
        }).catch(function (e) {
            reject(e);
        })
    })
};

const addReview = ({bookId, review, userId, rate}) => {
    return new Promise((resolve, reject) => {
        const query =
            `INSERT into book_reviews (book_id, review, reviewer_id, rate)
    VALUES ($1, $2, $3, $4)
    RETURNING book_id;`

        const params = [bookId, review, userId, rate];
        return db.query(query, params).then(function (results) {
            resolve(results.rows[0]);
        }).catch(function (e) {
            reject(e);
        })
    })
};

const getAllBooks = () => {
    return new Promise(function (resolve, reject) {
        const query = `SELECT books.*, users.username, users.created_at FROM books
                    JOIN users
                    ON (users.id = books.creator_id);`;
        return db.query(query).then(function (results) {
            resolve(results.rows[0]);
        }).catch(function (e) {
            reject(e);
        })
    })
};


const getReviews = () => {
    return new Promise(function (resolve, reject) {
        const query = `SELECT book_reviews.*, users.username FROM book_reviews
        JOIN users
        ON (users.id = book_reviews.reviewer_id);`;
        return db.query(query).then(function (results) {
            resolve(results.rows);
        }).catch(function (e) {
            reject(e);
        })
    })
};


function getBooks() {
    return new Promise(function (resolve, reject) {
        const q = `SELECT books.*, users.username
        FROM books
        JOIN users
        ON (users.id = books.creator_id);`

        return db.query(q).then(function (results) {
            resolve(results.rows);
        }).catch(function (e) {
            reject(e);
        })
    })
}

module.exports.login = login;
module.exports.registerUser = registerUser;
module.exports.addBook = addBook;
module.exports.getAllBooks = getAllBooks;
module.exports.editReview = editReview;
module.exports.addReview = addReview;
module.exports.getReviews = getReviews;
module.exports.getBooks = getBooks;

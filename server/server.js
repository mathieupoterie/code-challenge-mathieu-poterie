const path = require('path');
const express = require('express');
const app = express();
const publicPath = path.join(__dirname, '..', 'public');
const port = process.env.PORT || 3000;
const db = require('./database/database');
const cookieSession = require('cookie-session');
const bodyParser = require('body-parser');


app.use(cookieSession({
    name: 'session',
    keys: ["this is my secret"],

    // Cookie Options
    maxAge: 24 * 60 * 60 * 1000 // 24 hours
}))

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use('/static', express.static(publicPath));

app.post('/logIn', (req, res) => {
    const userName = req.body.userName;
    db.login(userName)
        .then(function (results) {
            if (!results) {
                db.registerUser(userName)
                    .then(function (results) {
                        req.session.user = results;
                        res.json({
                            success: true,
                            user: results
                        })
                    })
                    .catch(function (err) {
                        console.log(err);
                    });
            } else {
                req.session.user = results;
                res.json({
                    success: true,
                    user: results
                })
            }
        })
        .catch(function (err) {
            res.redirect('/login');
        })
});

app.get('/logOut', (req, res) => {
    req.session.user = null;
    res.redirect('/welcome');
});


app.get('/getUserInfo', (req, res) => {
    res.json({
        success: true,
        user: req.session.user
    })
});


app.get('/getReviews', (req, res) => {
    db.getReviews()
        .then((results) => {
            let reviews = {};
            results.map((result) => {
                const {created_at, review, rate, reviewer_id, book_id, username} = result;
                if (reviews[book_id]) {
                    reviews[book_id] = [...reviews[book_id], {
                        created_at,
                        review,
                        rate,
                        reviewer_id,
                        username,
                        bookId: book_id
                    }]
                } else {
                    reviews[book_id] = [{created_at, review, rate, reviewer_id, username, bookId: book_id}]
                }
            });
            res.json({
                success: true,
                reviews
            })
        })
});

app.get('/allBooks', (req, res) => {
    db.getBooks()
        .then((results) => {
            res.json({
                success: true,
                books: results
            })
        }).catch((e) => console.log(e))
})

app.post('/addNewBook', (req, res) => {
    db.addBook(req.body)
        .then((results) => {
            res.json({
                success: true,
                bookId: results['book_id'],
                username: req.session.user.username
            })
        }).catch((e) => console.log(e))
})


app.post('/addReview', (req, res) => {
    db.addReview(req.body)
        .then((results) => {
            res.json({
                success: true
            })
        }).catch((e) => console.log(e))
});

app.post('/editReview', (req, res) => {
    db.editReview(req.body)
        .then((results) => {
            res.json({
                success: true
            })
        }).catch((e) => console.log(e))
})


// WHERE LOGGED OUT
app.get('/welcome', (req, res) => {
    if (req.session.user) {
        return res.redirect('/');
    }
    res.sendFile(path.join(publicPath, 'index.html'));
});


// WHERE LOGGED IN
app.get('*', (req, res) => {
    if (!req.session.user) {
        return res.redirect('/welcome');
    }
    res.sendFile(path.join(publicPath, 'index.html'));
});


app.listen(port, () => {
    console.log('Listening on port 3000!');
});


module.exports = app;
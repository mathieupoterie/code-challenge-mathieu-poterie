import React from 'react';
import {Router, Route, IndexRoute, Switch, Link, NavLink} from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import MyBooks from '../components/MyBooks/MyBooks';
import UserBooks from '../components/MyBooks/UserBooks';
import AllBooks from '../components/AllBooks/AllBooks';
import NotFoundPage from '../components/NotFoundPage/NotFoundPage';
import Welcome from '../components/Welcome/Welcome';
import ReviewsPage from '../components/Reviews/ReviewsPage';
import PrivateRoute from './PrivateRoute';

const history = createHistory();

const userIsLoggedIn = location.pathname != '/welcome';

const loggedOut = (
    <Router history={history}>
        <Route path="/" component={Welcome}></Route>
    </Router>
);

const loggedIn = (
    <Router history={history}>
        <div>
            <Switch>
                <PrivateRoute path="/" component={MyBooks} exact={true}/>
                <PrivateRoute path="/create" component={UserBooks}/>
                <PrivateRoute path="/books" component={AllBooks}/>
                <PrivateRoute path="/book/:id/reviews" component={ReviewsPage}/>
                <Route component={NotFoundPage}/>
            </Switch>
        </div>
    </Router>
);


let AppRouter = loggedOut;
if (userIsLoggedIn) {
    AppRouter = loggedIn
}


export {AppRouter, history};

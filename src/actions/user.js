import {getUserInfo} from '../httpRequest/axios';

export const setUserInfo = (user) => ({
    type: 'SET_USER_INFO',
    user
})

export const startSetUserInfo = () => {
    return (dispatch) => {
        getUserInfo()
            .then((result) => {
                let user = result.data.user;
                return dispatch(setUserInfo(user));
            })
    }
};

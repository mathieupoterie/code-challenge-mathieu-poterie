import {addNewBook, getBooks} from '../httpRequest/axios';
import {addReview} from './reviews';


// ADD_BOOK
export const addBook = (book) => {
    return {
        type: 'ADD_BOOK',
        book
    }
}


export const startAddBook = (userId, username, newBook) => {
    return (dispatch) => {
        addNewBook(userId, newBook)
            .then((result) => {
                const {isbn, title, rate, review} = newBook;
                dispatch(addBook({
                    id: result.data.bookId,
                    creator_id: userId,
                    username,
                    isbn: `${isbn}`,
                    title
                }));
                dispatch(addReview({reviewer_id : userId, username, bookId: result.data.bookId, rate, review}));
            }).catch((e) => console.log(e));
    };
};


// SET_BOOKS
export const setBooks = (books) => ({
    type: 'SET_BOOKS',
    books
});


export const startSetBooks = () => {
    return (dispatch) => {
        getBooks()
            .then((response) => {
                dispatch(setBooks(response.data.books))
            })
            .catch((e) => console.log(e));
    }
};
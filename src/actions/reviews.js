import {editExistingReview, getReviews, addNewReview} from "../httpRequest/axios";


// SET REVIEWS
export const setReviews = (reviews) => {
    return {
        type: 'SET_REVIEWS',
        reviews
    }
};


export const startSetReviews = () => {
    return (dispatch) => {
        getReviews()
            .then((result) => {
                const reviews = result.data.reviews;
                return dispatch(setReviews(reviews));
            }).catch((e) => console.log(e));
    }
};

export const startEditReview = (userId, bookId, editedReview) => {
    return (dispatch) => {
        editExistingReview(userId, bookId, editedReview)
            .then(() => {
                dispatch(startSetReviews());
            }).catch((e) => console.log(e));
    };
};


export const addReview = (review) => {
    return {
        type: 'ADD_REVIEW',
        review
    }
};

export const startAddReview = (userId, username, bookId, newReview) => {
    return (dispatch) => {
        addNewReview(userId, bookId, newReview)
            .then(() => {
                const {rate, review} = newReview;
                dispatch(addReview({reviewer_id : userId, username, bookId, rate, review}))
            }).catch((e) => console.log(e));
    };
};
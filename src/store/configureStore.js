import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import booksReducer from '../reducers/books';
import filtersReducer from '../reducers/filters';
import reviewsReducer from '../reducers/reviews';
import userReducer from '../reducers/user';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
    const store = createStore(
        combineReducers({
            books: booksReducer,
            filters: filtersReducer,
            user: userReducer,
            reviews: reviewsReducer
        }),
        composeEnhancers(applyMiddleware(thunk))
    );
    return store;
};

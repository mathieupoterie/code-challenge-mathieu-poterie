import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {AppRouter} from './routers/AppRouter';
import configureStore from './store/configureStore';
import {startSetUserInfo} from './actions/user';
import {startSetBooks} from './actions/books';
import {startSetReviews} from './actions/reviews';
import 'normalize.css/normalize.css';
import './styles/styles.scss';
import 'font-awesome/css/font-awesome.min.css';
import 'react-dates/lib/css/_datepicker.css';


const store = configureStore();
const jsx = (
    <Provider store={store}>
        {AppRouter}
    </Provider>
);

ReactDOM.render(jsx, document.getElementById('app'));

export const InitializeStore = () => {
    store.dispatch(startSetUserInfo());
    store.dispatch(startSetBooks());
    store.dispatch(startSetReviews());
};

InitializeStore();

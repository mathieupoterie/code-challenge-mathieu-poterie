export const selectBooks = (books, {text}) => {
    if (Array.isArray(books)) {
        return books.filter((book) => {
            const textMatch = book.title.toLowerCase().includes(text.toLowerCase());
            return textMatch;
        }).sort((a, b) => {
            return a.rate < b.rate ? 1 : -1;
        });
    } else {
        return books;
    }
};

export const selectUsersBooks = (userId, books) => {
    return books.filter((book) => book.creator_id === userId);
};

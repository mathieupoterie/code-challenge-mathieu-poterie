import { addBook, setBooks } from '../../actions/books';
import books from '../fixtures/books';

test('should setup add book action object', () => {
    const action = addBook(books[0]);
    expect(action).toEqual({
        type: 'ADD_BOOK',
        book: books[0]
    });
});

test('should setup set books action object', () => {
    const action = setBooks(books);
    expect(action).toEqual({
        type: 'SET_BOOKS',
        books: books
    });
});
import { setReviews } from '../../actions/reviews';
import reviews from '../fixtures/reviews';

test('should setup set reviews action object', () => {
    const action = setReviews(reviews);
    expect(action).toEqual({
        type: 'SET_REVIEWS',
        reviews: reviews
    });
});
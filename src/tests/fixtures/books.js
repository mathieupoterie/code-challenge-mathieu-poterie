export default [
    {
        id: 1,
        isbn: 7978712121212,
        title: "La Chute",
        creator_id: 1,
        username: "Mathieu"
    },
    {
        id: 2,
        isbn: 9879779871212,
        title: "L'Etranger",
        creator_id: 2,
        username: "Capucine"
    }
]
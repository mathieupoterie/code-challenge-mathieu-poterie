export default {
    1: [{
        created_at: "2018-03-18T16:47:11.467Z",
        review: "Lovely Book!",
        rate: "4.5",
        reviewer_id: 2,
        username: "Capucine",
        bookId: 1
    }],
    2: [{
        created_at: "2018-03-18T16:47:11.467Z",
        review: "Pretty good Book!",
        rate: "3.5",
        reviewer_id: 2,
        username: "Capucine",
        bookId: 2
    }, {
        created_at: "2018-03-18T16:47:11.467Z",
        review: "Great Book!",
        rate: "5",
        reviewer_id: 1,
        username: "Mathieu",
        bookId: 2
    }]
}
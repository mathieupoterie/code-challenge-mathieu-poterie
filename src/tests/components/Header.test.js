import React from 'react';
import { shallow } from 'enzyme';
import {Header} from '../../components/Header/Header';
import user from '../fixtures/user'

test('should render Header correctly', () => {
  const wrapper = shallow(<Header user={user} />);
  expect(wrapper).toMatchSnapshot();
});
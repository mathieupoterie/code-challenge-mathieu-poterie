import React from 'react';
import { shallow } from 'enzyme';
import BookForm from '../../components/SharedComponents/BookForm';
import books from '../fixtures/books';

test('should render BookForm correctly', () => {
  const wrapper = shallow(<BookForm />);
  expect(wrapper).toMatchSnapshot();
});

test('should render BookForm correctly with books data', () => {
  const wrapper = shallow(<BookForm books={books[1]} />);
  expect(wrapper).toMatchSnapshot();
});

test('should render error for invalid form submission', () => {
  const wrapper = shallow(<BookForm />);
  expect(wrapper).toMatchSnapshot();
  wrapper.find('form').simulate('submit', {
    preventDefault: () => { }
  });
  expect(wrapper.state('error').length).toBeGreaterThan(0);
  expect(wrapper).toMatchSnapshot();
});

test('should set title on input change', () => {
  const value = 'La Chute';
  const wrapper = shallow(<BookForm />);
  wrapper.find('input').at(0).simulate('change', {
    target: { value }
  });
  expect(wrapper.state('title')).toBe(value);
});
import {Link} from 'react-router';
import React from 'react';
import {logIn} from '../../httpRequest/axios'

export default class Login extends React.Component {
    state = {};
    handleSubmit = (e) => {
        e.preventDefault();
        const {userName} = this.state;
        logIn({userName})
            .then((resp) => {
                if (resp.data.success) {
                    location.href = '/'
                }
            })
            .catch((e) => {
                console.log(e, "error promises");
            })
    }
    handleInput = (e) => {
        this.state[e.target.name] = e.target.value;
    }

    render() {
        return (
            <div className="box-layout">
                <form className="box-layout__box" onSubmit={this.handleSubmit}>
                    <h2 className="box-layout__title">Log In</h2>
                    {this.state.error && <p className="error-message">{this.state.message}</p>}
                    <input type="text" name="userName" onChange={this.handleInput}/>
                    <p>User Name</p>
                    <button className="button welcome-button" type="submit">Sign In</button>
                </form>
            </div>

        )
    }
}

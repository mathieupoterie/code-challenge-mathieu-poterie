import LogIn from './LogIn';
import React from 'react';

export default () => (
    <div>
        <div id="welcome-background"></div>
        <LogIn/>
    </div>
)

import React from 'react';
import {Link} from 'react-router-dom';
import Rating from 'react-rating';

const BookListItem = ({title, id, review, hasReviewedBook, hasAddedBook, reviewsCount, averageRate, user, creatorName}) => (
    <Link className="list-item" to={`/book/${id}/reviews`}>
        <div>
            <h3 className="list-item__title">{title}</h3>
            {hasAddedBook ? <p>You added this book</p> : <p>Added by {creatorName}</p>}
            {hasReviewedBook
                ? <p>You reviewed it</p>
                : <p>You did not review it</p>
            }
        </div>
        <div>
            {reviewsCount == 0
                ? <p> No reviews yet </p>
                : <p> {reviewsCount} review(s)</p>
            }
        </div>
        {
            averageRate ?
                <div>
                    <Rating
                        emptySymbol="fa fa-star-o fa-2x"
                        fullSymbol="fa fa-star fa-2x"
                        initialRating={parseFloat(Math.round((averageRate) * 2) * 0.5)}
                        fractions={2}
                        style={{fontSize: 10}}
                        readonly={true}
                    />
                    <p> AverageRate: {averageRate}</p>
                </div>
                : <p>No Rate given</p>
        }

    </Link>
);

export default BookListItem;

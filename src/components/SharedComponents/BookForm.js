import Rate from 'rc-rate';
import React from 'react';

export default class BookForm extends React.Component {
    constructor(props) {
        super(props);
            this.state = {
            title: props.book ? props.book.title : '',
            review: props.userReview && props.userReview[0] ? props.userReview[0].review : '',
            isbn: props.book ? props.book.isbn : '',
            rate: props.userReview && props.userReview[0]? props.userReview[0].rate : 0,
            error: '',
            ISBNAdvice: '',
            isEnabled: props.book ? true : false
        };
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.book) {
            const {title, isbn} = nextProps.book;
            this.setState({title, isbn, isEnabled: true});
        }
        if(nextProps.userReview && nextProps.userReview.length > 0) {
            const {rate, review} = nextProps.userReview[0];
            this.setState({review, rate});
        }
    };

    onTitleChange = (e) => {
        const title = e.target.value;
        this.setState(() => ({title}));
    };

    onReviewChange = (e) => {
        const review = e.target.value;
        this.setState(() => ({review}));
    };

    onRateChange = (rate) => {
        this.setState(() => ({rate}));
    };

    onisbnChange = (e) => {
        const isbn = e.target.value;
        if (!isbn || isbn.match(/^\d{1,13}$/)) {
            let ISBNAdvice, isISBNValid, isEnabled;
            const validISBN = `You entered a valid ISBN Number (${isbn.length} numbers)`;
            const notValidISBN = `Your ISBN number should be contains 10 or 13 numbers you are currently at ${isbn.length} numbers.`
            if (isbn.length === 10 || isbn.length === 13) {
                ISBNAdvice = validISBN;
                isISBNValid = 'form__success';
                isEnabled = true;
                this.props.books.map((book) => {
                    if (isbn === book.isbn) {
                        ISBNAdvice = "This ISBN Number already exist..."
                        isISBNValid = 'form__error'
                        isEnabled = false;
                    }
                });
            } else {
                ISBNAdvice = notValidISBN;
                isISBNValid = 'form__error'
                isEnabled = false;
            }
            ;
            this.setState(() => ({isbn, ISBNAdvice, isISBNValid, isEnabled}));
        }
    };

    onSubmit = (e) => {
        e.preventDefault();
        this.setState({isEnabled : false});
        if (!this.state.title || !this.state.isbn) {
            const error = "Please fill the book title or/and ISBN Number!"
            this.setState(() => ({error}))
        } else {
            this.setState(() => ({error: ''}))
            this.props.onSubmit({
                title: this.state.title,
                isbn: parseFloat(this.state.isbn, 10),
                rate: this.state.rate,
                review: this.state.review
            })
        }
    };

    render() {
        return (
            <form className="form" onSubmit={this.onSubmit}>
                {this.state.error && <p className="form__error">{this.state.error}</p>}
                <input
                    type="text"
                    placeholder="Book Title"
                    autoFocus
                    className="text-input"
                    value={this.state.title}
                    onChange={this.onTitleChange}
                    disabled={this.props.book ? true : false}
                />
                <input
                    type="text"
                    placeholder="ISBN number"
                    className="text-input"
                    value={this.state.isbn}
                    onChange={this.onisbnChange}
                    disabled={this.props.book ? true : false}
                />
                {this.state.ISBNAdvice && <p className={this.state.isISBNValid}>{this.state.ISBNAdvice}</p>}
                <Rate
                    defaultValue={parseFloat(this.state.rate, 10)}
                    value={parseFloat(this.state.rate, 10)}
                    onChange={this.onRateChange}
                    style={{fontSize: 40}}
                    allowHalf
                    allowClear={true}
                />
                <textarea
                    placeholder="Add a review for this book (optional)"
                    className="textarea"
                    value={this.state.review}
                    onChange={this.onReviewChange}
                >
                </textarea>
                <div>
                    <button className="button" disabled={!this.state.isEnabled}>Save Book</button>
                </div>
            </form>
        )
    }
}

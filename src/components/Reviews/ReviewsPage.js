import React from 'react';
import {connect} from 'react-redux';
import moment from 'moment';
import {getUserReview} from '../../selectors/reviews';
import Rating from 'react-rating';
import BookForm from '../SharedComponents/BookForm';
import {startEditReview, startAddReview} from '../../actions/reviews';


export class ReviewsPage extends React.Component {
    constructor(props) {
        super(props);
    }

    onSubmit = ({ rate, review}) => {
        if(this.props.userReview && this.props.userReview.length > 0) {
            this.props.startEditReview(this.props.user.id, this.props.book.id, { rate, review});
        } else {
            this.props.startAddReview(this.props.user.id, this.props.user.username, this.props.book.id, {rate, review});
        }
        this.props.history.push('/books');
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.book && nextProps.userReview && nextProps.userReview.length > 0) {
            const {title, isbn} = nextProps.book;
            const {rate, review} = nextProps.userReview[0];
            this.setState({title, review, isbn, rate, isEnabled: true});
        }
    };

    render() {
        return (
            <div>
                <div className="page-header">
                    <div className="content-container">
                        {this.props.book && <h1 className="page-header__title">Review for the book '{this.props.book.title}'</h1>}
                    </div>

                </div>
                <div className="content-container">
                    <div className="list-header">
                        <div className="show-for-mobile">Review(s)</div>
                        <div className="show-for-desktop">Review(s)</div>
                    </div>
                    <div className="list-body">

                        {
                            !(this.props.book && this.props.bookReviews) ? (
                                <div className="list-item list-item--message">
                                    <span>No Reviews For This Book</span>
                                </div>
                            ) : (
                                this.props.bookReviews.map(({created_at, rate, review, username, reviewer_id}) => {
                                    return (
                                        <div className="list-item list-item--message review-container" key={reviewer_id}>
                                            <div className="reviewer">
                                                <p>{username ? username : 'You'}</p>
                                                { created_at ? <p>{moment(created_at).utc().format('YYYY-MM-DD')}</p> : <p>Just created</p>}
                                                <Rating
                                                    emptySymbol="fa fa-star-o fa-2x"
                                                    fullSymbol="fa fa-star fa-2x"
                                                    initialRating={parseFloat(rate)}
                                                    fractions={2}
                                                    style={{fontSize: 10}}
                                                    readonly={true}
                                                />
                                            </div>
                                            <p className="review"><em>"{review}"</em></p>
                                        </div>
                                    )
                                })
                            )
                        }
                    </div>
                </div>

                <div className="content-container">
                    <h1>{this.props.userReview && this.props.userReview.length > 0 ? 'Edit your review' : 'Add a review'}</h1>
                    <BookForm
                        book={this.props.book}
                        userReview={this.props.userReview}
                        books={this.props.books}
                        onSubmit={this.onSubmit}
                    />
                </div>
            </div>
        )
    }
}



const mapStateToProps = (state, props) => {
    const book = state.books.find((book) => book.id == props.match.params.id && book);
    let userReview = undefined;
    if (state.reviews && state.reviews[props.match.params.id]) {
        userReview = getUserReview(state.reviews[props.match.params.id], state.user.id);
    }
    return {
        book,
        userReview,
        user: state.user,
        bookReviews: state.reviews[props.match.params.id]
    };
};


const mapDispatchToProps = (dispatch, props) => ({
    startEditReview: (userId, bookId, editedReview) => dispatch(startEditReview(userId, bookId, editedReview)),
    startAddReview: (userId, username, bookId, editedReview) => dispatch(startAddReview(userId, username, bookId, editedReview))
})

export default connect(mapStateToProps, mapDispatchToProps)(ReviewsPage);

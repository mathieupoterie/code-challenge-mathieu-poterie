import React from 'react';
import {connect} from 'react-redux';
import {setTextFilter} from '../../actions/filters';


class AllBooksSummary extends React.Component {
    state = {
        bookWord: this.props.books.length <= 1 ? 'book' : 'books'
    };

    onTextChange = (e) => {
        this.props.setTextFilter(e.target.value);
    };

    render() {
        return (
            <div className="page-header">
                <div className="content-container all-books_title">
                    <h1 className="page-header__title">Viewing <span>{this.props.books.length}</span> {this.state.bookWord}
                    </h1>
                    <input type="text" placeholder="Search" onChange={this.onTextChange}/>
                </div>
            </div>
        );
    }
};

const mapDispatchToProps = (dispatch, props) => ({
    setTextFilter: (text) => dispatch(setTextFilter(text))
});

export default connect(undefined, mapDispatchToProps)(AllBooksSummary);

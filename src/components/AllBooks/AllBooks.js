import React from 'react';
import {connect} from 'react-redux';
import AllBooksList from './AllBooksList';
import AllBooksSummary from './AllBooksSummary';
import {selectBooks} from '../../selectors/books';

const AllBooks = ({books, filters, user, reviews}) => (
    <div>
        <AllBooksSummary books={books} filters={filters}/>
        <AllBooksList books={books} user={user} reviews={reviews}/>
    </div>
)

const mapStateToProps = (state) => {
    return {
        books: selectBooks(state.books, state.filters),
        filters: state.filters,
        reviews: state.reviews,
        user: state.user
    };
};

export default connect(mapStateToProps)(AllBooks);

import React from 'react';
import BookListItem from '../SharedComponents/BookListItem';

const AllBooksList = (props) => (
    <div className="content-container">
        <div className="list-header">
            <div className="show-for-mobile">Books</div>
            <div className="show-for-desktop">Book</div>
            <div className="show-for-desktop">Rate</div>
        </div>
        <div className="list-body">
            {
                props.books.length === 0 ? (
                    <div className="list-item list-item--message">
                        <span>No Books</span>
                    </div>
                ) : (
                    props.reviews &&
                    props.books.map((book) => {
                        let hasReviewedBook = false;
                        let userRate = undefined;
                        let hasAddedBook= false;
                        if (props.user.id == book.creator_id) {
                                    hasAddedBook = true;
                        }
                        let averageRate = undefined;
                        let ratesSums = 0;
                        let matchingReviews = props.reviews[book.id] ? props.reviews[book.id] : [];
                        matchingReviews.map((review) => {
                            ratesSums += parseFloat(review.rate);
                            if (review.reviewer_id == props.user.id) {
                                userRate = review.rate;
                                hasReviewedBook = true;
                            }
                        });
                        averageRate = props.reviews[book.id] && (ratesSums / matchingReviews.length).toFixed(2);
                        return <BookListItem
                            key={book.id}
                            {...book}
                            user={props.user}
                            hasAddedBook={hasAddedBook}
                            hasReviewedBook={hasReviewedBook}
                            reviewsCount={matchingReviews.length}
                            averageRate={averageRate}
                            creatorName={book.username}
                            myRate={userRate}
                        />
                    })
                )
            }
        </div>
    </div>
);

export default AllBooksList;

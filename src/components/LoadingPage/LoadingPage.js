import React from 'react';

const LoadingPage = () => (
    <div className="loader">
        <img className="loader__image" src="/static/images/loader.gif"/>
    </div>
);

export default LoadingPage;

import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {logOut} from '../../httpRequest/axios';

 export class Header extends React.Component {
    startLogOut = () => {
        logOut()
            .then(() => {
                location.href = '/';
            })
            .catch((e) => {
                console.log(e, "error promises");
            })
    };

    render() {
        return (
            <header className="header">
                <div className="content-container">
                    <div className="header__content">
                        <div className="header__title">Hi {this.props.user.username} !</div>
                        <Link className="header__title" to="/">
                            <p>My Books</p>
                        </Link>
                        <Link className="header__title" to="/books">
                            <p>All Books</p>
                        </Link>
                        <p className="button--link log-out-button" onClick={this.startLogOut}>Log Out</p>
                    </div>
                </div>
            </header>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
};


export default connect(mapStateToProps)(Header);


import React from 'react';
import {Link} from 'react-router-dom';

const BookSummary = ({bookCount, books}) => {
    const bookWord = bookCount <= 1 ? 'book' : 'books';
    return (
        <div className="page-header">
            <div className="content-container">
                <h1 className="page-header__title">Viewing <span>{books.length}</span> {bookWord}</h1>
                <div className="page-header__actions">
                    <Link className="button" to="/create">Add Book</Link>
                </div>
            </div>
        </div>
    );
};

export default BookSummary;

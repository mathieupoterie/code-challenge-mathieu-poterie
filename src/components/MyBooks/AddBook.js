import React from 'react';
import {connect} from 'react-redux';
import BookForm from '../SharedComponents/BookForm';
import {startAddBook} from '../../actions/books';

export class AddBookPage extends React.Component {
    onSubmit = (book) => {
        this.props.startAddBook(this.props.user.id, this.props.user.username, book);
        this.props.history.push('/');
    }

    render() {
        return (
            <div>
                <div className="page-header">
                    <div className="content-container">
                        <h1 className="page-header__title">Add Book</h1>
                    </div>
                </div>
                <div className="content-container">
                    <BookForm
                        books={this.props.books}
                        onSubmit={this.onSubmit}
                    />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, props) => {
    return {
        user: state.user,
        books: state.books
    };
};

const mapDispatchToProps = (dispatch) => ({
        startAddBook: (userId, username, book) => dispatch(startAddBook(userId, username, book))
    }
);

export default connect(mapStateToProps, mapDispatchToProps)(AddBookPage);

import React from 'react';
import MyBookList from './MyBookList';
import MyBooksSummary from './MyBooksSummary';
import {connect} from "react-redux";
import {selectUsersBooks} from "../../selectors/books";

const MyBook = ({bookCount, books, user, reviews}) => (
    <div>
        <MyBooksSummary books={books}/>
        <MyBookList books={books} user={user} reviews={reviews}/>
    </div>
)

const mapStateToProps = (state) => {
    const visibleBooks = Array.isArray(state.books) ? selectUsersBooks(state.user.id, state.books) : state.books;
    return {
        books: visibleBooks,
        filters: state.filters,
        reviews: state.reviews,
        user: state.user
    }
};


export default connect(mapStateToProps)(MyBook);

import React from 'react';
import {connect} from 'react-redux';
import BookListItem from '../SharedComponents/BookListItem';
import {selectUsersBooks} from '../../selectors/books';

const MyBookList = (props) => (
    <div className="content-container">
        <div className="list-header">
            <div className="show-for-mobile">Books</div>
            <div className="show-for-desktop">Book</div>
            <div className="show-for-desktop">Rate</div>
        </div>
        <div className="list-body">
            {
                props.books.length === 0 ? (
                    <div className="list-item list-item--message">
                        <span>No Books</span>
                    </div>
                ) : (

                    props.reviews &&
                    props.books.map((book) => {
                        let myRate = undefined;
                        let hasReviewedBook = false;
                        let ratesSums = 0;
                        let matchingReviews = props.reviews[book.id] ? props.reviews[book.id] : [];
                        matchingReviews.map((review) => {
                            ratesSums += parseFloat(review.rate);
                            if (review.reviewer_id == props.user.id) {
                                myRate = review.rate;
                                hasReviewedBook = true;
                            }
                        });
                        let averageRate = props.reviews[book.id] && (ratesSums / matchingReviews.length).toFixed(2);
                        return <BookListItem
                            key={book.id}
                            {...book}
                            user={props.user}
                            hasAddedBook={true}
                            hasReviewedBook={hasReviewedBook}
                            reviewsCount={matchingReviews.length}
                            averageRate={averageRate}
                            myRate={myRate}
                        />
                    })

                )
            }


        </div>
    </div>
)

export default MyBookList;

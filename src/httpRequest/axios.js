import axios from 'axios';

// LOGIN / LOGOUT
export const logIn = (userName) => {
    return new Promise((resolve, reject) => {
        return axios.post('/LogIn', userName)
            .then((response) => resolve(response))
            .catch((e) => reject(e));
    });
};

export const logOut = () => {
    return new Promise((resolve, reject) => {
        return axios.get('/logOut')
            .then((response) => resolve(response))
            .catch((e) => reject(e));
    });
};

// USER
export const getUserInfo = () => {
    return new Promise((resolve, reject) => {
        return axios.get('/getUserInfo')
            .then((response) => resolve(response))
            .catch((e) => reject(e));
    });
}


// BOOK
export const addNewBook = (userId, newBook) => {
    return new Promise((resolve, reject) => {
        return axios.post('/addNewBook', {userId, ...newBook})
            .then((response) => resolve(response))
            .catch((e) => reject(e));
    });
};

export const getBooks = () => {
    return new Promise((resolve, reject) => {
        return axios.get('/allBooks')
            .then((response) => resolve(response))
            .catch((e) => reject(e))
    });
}


// REVIEWS
export const editExistingReview = (userId, bookId, editedReview) => {
    return new Promise((resolve, reject) => {
        return axios.post('/editReview', {userId, bookId, ...editedReview})
            .then((response) => resolve(response))
            .catch((e) => reject(e));
    });
};

export const addNewReview = (userId, bookId, editedReview) => {
    return new Promise((resolve, reject) => {
        return axios.post('/addReview', {userId, bookId, ...editedReview})
            .then((response) => resolve(response))
            .catch((e) => reject(e));
    });
};


export const getReviews = () => {
    return new Promise((resolve, reject) => {
        return axios.get('/getReviews')
            .then((response) => resolve(response))
            .catch((e) => reject(e));
    });
};


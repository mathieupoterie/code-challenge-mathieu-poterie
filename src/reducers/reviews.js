const reviewsReducerDefaultState = {};

export default (state = reviewsReducerDefaultState, action) => {
    switch (action.type) {
        case 'SET_REVIEWS':
            return {
                ...state,
                ...action.reviews
            };
        case 'ADD_REVIEW':
            let updatedIndex = undefined;
            if (state[action.review.bookId]) {
                updatedIndex = {
                    [action.review.bookId]: [...state[action.review.bookId], action.review]
                }
            } else {
                updatedIndex = {
                    [action.review.bookId]: [action.review]
                }
            }
            return {
                ...state,
                ...updatedIndex
            }
        default:
            return state;
    }
};

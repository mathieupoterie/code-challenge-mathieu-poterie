# YARN Commands

After cloning the repo, at the root of the project,  install the dependencies

```
yarn 
```

You will need to set up your local postgresql database.
```
createdb codechallengemathieu
```

Then run your schema.sql (the tables will be created and you will have movies in your database !).
```
psql codechallengemathieu -f schema.sql
```



Then run two commands:
<br/>
<br/>
Start the node server (port 3000 locally):



```
yarn start
```

And build your code.

```
yarn run build:dev --watch
```

